public class Web
{
    public static org.reactivestreams.Publisher<java.lang.Void> list(final reactor.netty.http.server.HttpServerRequest request, final reactor.netty.http.server.HttpServerResponse response)
    {
        final var objectMapper = new com.fasterxml.jackson.databind.ObjectMapper();
        final var client = java.net.http.HttpClient.newHttpClient();
        try
        {
            java.lang.System.out.println(objectMapper.readTree(client.sendAsync(java.net.http.HttpRequest.newBuilder(java.net.URI.create("https://huggingface.co/api/models/chaowenguo/video")).build(), java.net.http.HttpResponse.BodyHandlers.ofString()).thenApply(java.net.http.HttpResponse::body).join()).get("siblings"));
        }
        catch (final java.lang.Exception e){}
        return response.send(request.receive().retain());
    }

    public static void main(final java.lang.String[] args)
    {
        final var server = reactor.netty.http.server.HttpServer.create().secure(spec -> spec.sslContext(reactor.netty.http.Http2SslContextSpec.forServer(new java.io.File("/etc/ssl/certs/cert.pem"), new java.io.File("/etc/ssl/private/key.pem")))).route(routes -> routes.post("/list", Web::list)).protocol(reactor.netty.http.HttpProtocol.H2).port(443).bindNow();
        server.onDispose().block();
    }
}